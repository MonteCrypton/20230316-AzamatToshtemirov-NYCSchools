package com.azamat.nycs.app

import android.app.Application
import com.azamat.nycs.util.SharedPreferences.initSharedPreferences


class NYCSchoolsApp : Application() {

    companion object {
        lateinit var INSTANCE: NYCSchoolsApp
        private val TAG = NYCSchoolsApp::class.java.simpleName
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        initSharedPreferences(TAG, this.applicationContext)
        KoinConfig.start(this)
    }

    override fun onTerminate() {
        super.onTerminate()
        KoinConfig.stop()
    }


}