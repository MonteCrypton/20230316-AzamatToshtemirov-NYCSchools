package com.azamat.nycs.model.remote.api

import com.azamat.nycs.model.Constants
import com.azamat.nycs.model.remote.response.School
import com.azamat.nycs.model.remote.response.Score
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET(Constants.NYCSEndpointUrls.SCHOOL_ATTRIBUTES)
    suspend fun getSchools(): List<School>

    @GET(Constants.NYCSEndpointUrls.SCORES_ATTRIBUTES)
    suspend fun getScores(): List<Score>

    @GET(Constants.NYCSEndpointUrls.SCORE_DBN_ATTRIBUTES)
    suspend fun getScoreByDbn(@Query("dbn") dbn: String): List<Score>
}