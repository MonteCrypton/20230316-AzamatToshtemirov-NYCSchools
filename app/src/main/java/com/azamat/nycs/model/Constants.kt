package com.azamat.nycs.model

object Constants {
    const val OkHttp_TIMEOUT = 5L // connection timeout

    object NYCSEndpointUrls{
        const val SCHOOL_ATTRIBUTES = "s3k6-pzi2.json"
        const val SCORES_ATTRIBUTES = "f9bf-2cp4.json"

        const val SCORE_DBN_ATTRIBUTES = "f9bf-2cp4.json"

    }
}