package com.azamat.nycs.model.repository.remote

import com.azamat.nycs.base.BaseApiResult
import com.azamat.nycs.model.remote.response.School
import com.azamat.nycs.model.remote.response.Score

interface RemoteRepository {

    suspend fun getSchools() : BaseApiResult<List<School>>

    suspend fun getScores() : BaseApiResult<List<Score>>

    suspend fun getScoreByDbn(dbn: String) : BaseApiResult<List<Score>>
}